import unittest


# Erweitern Sie die Klasse und Konto und deren Tests.
# Nutzen Sie die Klasse TestKonto für die erstellten Tests.
# Die Tests können über die Konsole mit python <Name_der_Datei>.py ausgeführt werden.
# Viel Spaß :-)!


class Konto(object):
    def __init__(self, inhaber, kontonummer,
                 kontostand,
                 kontokorrent=0):
        self.__Inhaber = inhaber
        self.__Kontonummer = kontonummer
        self.__Kontostand = kontostand
        self.__Kontokorrent = kontokorrent

    def ueberweisen(self, ziel, betrag):
        if (self.__Kontostand - betrag < -self.Kontokorrent):
            # Deckung nicht genuegend
            return False
        else:
            self.__Kontostand -= betrag
            ziel.__Kontostand += betrag
            return True

    def einzahlen(self, betrag):
        self.__Kontostand += betrag

    def auszahlen(self, betrag):
        self.__Kontostand -= betrag

    def get_kontostand(self):
        return self.__Kontostand

    def get_kontonummer(self):
        return self.__Kontonummer


class TestKonto(unittest.TestCase):

# Erste Übung: Der folgende Test schlägt fehl? Warum eigentlich? Denken Sie kurz mal drüber nach...
# Der Test schlägt fehl, weil die Methode die IBANs nicht übereinstimmen.
# Ergänzen Sie die Methode "set_kontonummer" in der Klasse Konto und
    def test_Kontonummer(self):
        # given
        konto = Konto("Mueller Claudia", "DE89370400440532013000", 1000.00)
        # when
        # konto.set_kontonummer("DE89370400440532555555")
        # then
        self.assertEquals("DE89370400440532555555", konto.get_kontonummer(), "IBANs stimmen nicht überein")

# Zweite Übung: Der folgende Test schlägt fehl? Warum eigentlich? Denken Sie kurz mal drüber nach...
# Der Test schlägt fehl, weil die Kontoinhaber nicht übereinstimmen. In der Klasse Konto
# kann der Kontoinhaber noch nicht ausgegeben oder verändert werden.
# Implementieren Sie die notwendigen Getter und Setter und implementieren Sie den Test.
    def test_Kontoinhaber(self):
        # given
        konto = Konto("Mueller Claudia", "DE89370400440532013000", 1000.00)
        # when
        # ??
        # then
        self.assertEquals("Mueller-Schulze Claudia", ()) #??, "Kontoinhaber stimmt nicht überein")

# Verbosity=2 sorgt dafür, dass die Ausgaben "wortreichender" sind.
# Wenn Sie den Level auf 1 ändern, erkennen Sie den Unterschied.
if __name__ == '__main__':
    unittest.main(verbosity=2)
